package formative2.model;

public class Price extends Product {
    private int id;
    private int ammount;
    private String valuta;
    private int produkId;
    private double total;

    public Price(int id,String name, String deskripsi, int ammount, String valuta){
        super(name,deskripsi);
        this.id = id;
        this.ammount = ammount;
        this.valuta = valuta;
        this.produkId = super.getId();
    }

    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public int getAmmount() {
        return ammount;
    }

    public String getValuta() {
        return valuta;
    }

    public int getProdukId() {
        return produkId;
    }

    public void setTotal(double total) {
        this.total = this.total + total ;
    }

    public double getTotal() {
        return total;
    }
}
