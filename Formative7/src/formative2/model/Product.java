package formative2.model;

public class Product {
    private int id = 0;
    private String name;
    private String description;

    public Product( String name, String description){
        id++;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
