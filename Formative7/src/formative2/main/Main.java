package formative2.main;

import formative2.model.Price;

import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        buy();
    }

    public static LinkedList<Price> allItem() {
        LinkedList<Price> listItem = new LinkedList<Price>();
        listItem.add(new Price(1, "Buku", "Ini buku sinar matahari", 3500, "IDR"));
        listItem.add(new Price(2, "Pensil", "Merek Faber kestel", 2000, "IDR"));
        listItem.add(new Price(3, "Pulpen", "hitecg", 6000, "IDR"));
        listItem.add(new Price(4, "Pengserut", "Merek Faber kestel", 4000, "IDR"));
        listItem.add(new Price(5, "Buku Gambar", "Merah", 3000, "IDR"));
        listItem.add(new Price(6, "Pensil Warna", "Warna Putih", 4500, "IDR"));
        listItem.add(new Price(7, "Penghapus", "Kuning", 2500, "IDR"));
        listItem.add(new Price(8, "Penggaris", "putih", 5500, "IDR"));
        listItem.add(new Price(9, "Gunting", "Biru", 6500, "IDR"));
        listItem.add(new Price(10, "Gunting Kuku", "Perak", 1500, "IDR"));

        return listItem;
    }

    public static void allProduk(LinkedList<Price> listItem) {
        for (var data : listItem) {
            System.out.println(data.getId() + "\t" + data.getName() + "\t" + data.getAmmount());
        }
    }

    public static void buy() {
        LinkedList cart = new LinkedList();
        Scanner input = new Scanner(System.in);

        boolean again = true;
        boolean ada = false;

        do {
            System.out.println("\n===== Selamat Datang Di BAMART=====\n");
            allProduk(allItem());
            System.out.println("apa yang ingin di beli? ");
            int item = input.nextInt();
            for (var data : allItem()) {
                if (item == data.getId()) {
                    System.out.println();
                    System.out.println("Anda memilih : " + data.getName());
                    System.out.print("mau beli berapa? ");
                    int buy = input.nextInt();
                    cart.add(data.getId() + "," + buy);
                    System.out.println("ada lagi ? : ");
                    System.out.println("1. ya");
                    System.out.println("2. tidak");
                    System.out.print("silakan pilih : ");
                    int lagi = input.nextInt();
                    again = lagi == 1;
                    ada = true;
                }
            }
            if (!ada) {
                System.out.println("Barang tidak ada :( ");
            }
        } while (again);

        bonusAndDiskon(cart);
    }

    public static void bonusAndDiskon(LinkedList cart) {
        LinkedList dataItem = new LinkedList();
        double total = 0;
        double harga = 0;
        String bonus = " ";

        for (var data : cart) {
            String[] item = data.toString().split(",");
            for (var belanja : allItem()) {
                if (Integer.parseInt(item[0]) == belanja.getId()) {
                    if (Integer.parseInt(item[1]) > 5) {
                        harga = belanja.getAmmount() * 0.2;
                        belanja.setTotal((belanja.getAmmount() * Integer.parseInt(item[1])) + (belanja.getTotal() - harga));
                        total = belanja.getTotal();
                    } else {
                        belanja.setTotal(belanja.getAmmount() * Integer.parseInt(item[1]));
                        total += belanja.getTotal();
                    }
                }
            }
            if (!dataItem.contains(item[0])) {
                dataItem.add(item[0]);
            }
        }

        double termurah = 0;
        if (dataItem.size() >= 5) {
            for (int a = 0; a < cart.size(); a++) {
                String[] item = cart.get(a).toString().split(",");
                for (var belanja : allItem()) {
                    if (Integer.parseInt(item[0]) == belanja.getId()) {
                        System.out.println(belanja.getId());
                        if (a == 0) {
                            termurah = belanja.getAmmount();
                        }
                        if (termurah > belanja.getAmmount()) {
                            termurah = belanja.getAmmount();
                            bonus = belanja.getName();
                        }
                    }
                }
            }
        }
        struk(cart, bonus, total);
    }

    public static void struk(LinkedList list, String bonus, double total) {
        String valuta = " ";
        System.out.println("\n ====== Struk ======");
        System.out.println("Barang yang engkau beli");
        for (var allitem : list) {
            String[] allItem = allitem.toString().split(",");
            for (var item : allItem()) {
                if (Integer.parseInt(allItem[0]) == item.getId()) {
                    System.out.println(item.getName() + " " + allItem[1] + "x" + " " + (item.getAmmount() * Integer.parseInt(allItem[1])));
                    valuta = item.getValuta();
                }
            }
        }

        System.out.println("total = " + total + " " + valuta);
        System.out.println("Anda mendapat gratis : " + bonus);

    }
}
