package formative1.main;

import formative1.model.Programmer;
import formative1.model.ProjectManager;
import formative1.model.SystemAnalys;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Programmer programmer = new Programmer();
        programmer.setJoin(LocalDate.of(2021, 2, 1));
        programmer.start();

        SystemAnalys systemAnalys = new SystemAnalys();
        systemAnalys.setJoin(LocalDate.of(2021, 3, 1));
        systemAnalys.start();

        ProjectManager projectManager = new ProjectManager();
        projectManager.setJoin(LocalDate.of(2021, 4, 1));
        projectManager.start();


    }
}
