package formative1.model;

import java.io.FileWriter;
import java.time.LocalDate;
import java.time.Period;

public class ProjectManager extends Thread implements Salary {
    String name = "Project Manager";
    int lebaran;

    int monthbetwen;

    void printSalary() {
        try {
            FileWriter textSalary = new FileWriter("ProjectManager.txt");
            StringBuilder text = new StringBuilder();
            for (int i = 0; i < getJoin(); i++) {
                if (i == getTHR()) {
                    text.append(name).append(" ").append((salary() * (monthbetwen + 1)) / 12).append(" THR\n");
                }
                text.append(name).append(" ").append(salary()).append(" Gaji \n");
            }
            textSalary.write(text.toString());
            textSalary.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getTHR() {
        return lebaran = 12 - monthbetwen;
    }

    public void setJoin(LocalDate join) {
        LocalDate year = LocalDate.of(2021, 12, 1);
        Period betwen = Period.between(join, year);
        monthbetwen = betwen.getMonths();
    }

    public int getJoin() {
        return monthbetwen;
    }

    @Override
    public void run() {
        printSalary();
    }

    @Override
    public int salary() {
        return 12000000;
    }
}
